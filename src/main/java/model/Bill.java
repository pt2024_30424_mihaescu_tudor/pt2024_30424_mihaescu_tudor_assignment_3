package model;

public record Bill(int id, int orderId, int clientId, int productId, int quantity, double totalPrice, java.time.LocalDateTime date) {

}