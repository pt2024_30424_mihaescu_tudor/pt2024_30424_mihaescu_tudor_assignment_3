package model;

import java.util.Date;

public class Order {

    private int id;
    private int id_client;
    private int id_product;
    private String delivery_address;
    private int quantity;
    private int total_price;

    public Order()
    {
    }

    public Order(int id_client, int id_product, String delivery_address, int quantity, int total_price) {
        this.id_client = id_client;
        this.id_product = id_product;
        this.delivery_address = delivery_address;
        this.quantity = quantity;
        this.total_price = total_price;
    }

    public int getId() {
        return id;
    }
    public int getId_client() {
        return id_client;
    }
    public int getId_product() {
        return id_product;
    }
    public String getDelivery_address() {
        return delivery_address;
    }
    public int getQuantity()
    {
        return quantity;
    }
    public int getTotal_price() {
        return total_price;
    }


    public void setId(int id){
        this.id = id;
    }
    public void setId_client(int id_client) {
        this.id_client = id_client;
    }
    public void setId_product(int id_product) {
        this.id_product = id_product;
    }
    public void setDelivery_address(String delivery_address) {
        this.delivery_address = delivery_address;
    }
    public void setQuantity(int quantity){this.quantity = quantity;}
    public void setTotal_price(int total_price) {
        this.total_price = total_price;
    }

}
