package bll;

import bll.validators.ValidateOrder;
import dao.OrderDAO;
import model.Bill;
import model.Client;
import model.Order;
import model.Product;

import javax.swing.*;
import java.time.LocalDateTime;
import java.util.List;

public class OrderBLL {
    private OrderDAO orderDAO;

    public OrderBLL()
    {
        this.orderDAO = new OrderDAO();
    }

    public List<Order> findAllOrder()
    {
        return orderDAO.findAll();
    }
    public Order findByIdOrder(int id)
    {
        return orderDAO.findById(id);
    }
    public boolean insertOrder(Order order)
    {
        ValidateOrder validateOrder = new ValidateOrder();
        if(validateOrder.validate(order))
        {
            ProductBLL productBLL = new ProductBLL();
            Product product = productBLL.findByIdProduct(order.getId_product());

            if(product.getStock() - order.getQuantity() < 0) {
                showError("The maximum number of items is " + product.getStock());
                return false;
            }
            else
            {
                product.setStock(product.getStock() - order.getQuantity());
            }

            order.setTotal_price(product.getPrice() * order.getQuantity());

            BillBLL billBLL = new BillBLL();
            Bill bill = new Bill(0, order.getId(), order.getId_client(), order.getId_product(), order.getQuantity(), order.getTotal_price(), LocalDateTime.now());
            billBLL.insertBill(bill);
            productBLL.updateProduct(product);
            orderDAO.insert(order);
            return true;
        }
        return false;
    }

    public boolean updateOrder(Order order)
    {
        ValidateOrder validateOrder = new ValidateOrder();
        if(validateOrder.validate(order))
        {
            Order initialOrder = this.findByIdOrder(order.getId());

            int quantityDiff = order.getQuantity() - initialOrder.getQuantity();

            ProductBLL productBLL = new ProductBLL();
            Product product = productBLL.findByIdProduct(order.getId_product());

            if (product.getStock() - quantityDiff < 0) {
                showError("The maximum number of items is " + product.getStock());
                return false;
            }
            else {
                product.setStock(product.getStock() - quantityDiff);
            }

            order.setTotal_price(product.getPrice() * order.getQuantity());


            BillBLL billBLL = new BillBLL();
            Bill bill = new Bill(0, order.getId(), order.getId_client(), order.getId_product(), order.getQuantity(), order.getTotal_price(), LocalDateTime.now());
            billBLL.insertBill(bill);

            productBLL.updateProduct(product);
            orderDAO.update(order);
            return true;
        }
        return false;
    }

    public void deleteOrder(Order order)
    {
        ValidateOrder validateOrder = new ValidateOrder();
        if(validateOrder.validate(order))
        {
            ProductBLL productBLL = new ProductBLL();
            Product product = productBLL.findByIdProduct(order.getId_product());

            product.setStock(product.getStock() + order.getQuantity());

            productBLL.updateProduct(product);
            orderDAO.delete(order);
        }
    }

    public void showError(String error)
    {
        JFrame showError = new JFrame();
        JOptionPane.showMessageDialog(showError, error);
    }

}
