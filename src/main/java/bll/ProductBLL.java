package bll;

import bll.validators.ValidateProduct;
import dao.ProductDAO;
import model.Client;
import model.Product;

import java.util.List;

public class ProductBLL {
    private ProductDAO productDAO;

    public List<Product> findAllProduct()
    {
        return productDAO.findAll();
    }
    public Product findByIdProduct(int id)
    {
        return productDAO.findById(id);
    }

    public ProductBLL()
    {
        this.productDAO = new ProductDAO();
    }

    public boolean insertProduct(Product product)
    {
        ValidateProduct validateProduct = new ValidateProduct();
        if(validateProduct.validate(product))
        {
            productDAO.insert(product);
            return true;
        }
        return false;
    }

    public boolean updateProduct(Product product)
    {
        ValidateProduct validateProduct = new ValidateProduct();
        if(validateProduct.validate(product))
        {
            productDAO.update(product);
            return true;
        }
        return false;
    }

    public void deleteProduct(Product product)
    {
        productDAO.delete(product);
    }

}