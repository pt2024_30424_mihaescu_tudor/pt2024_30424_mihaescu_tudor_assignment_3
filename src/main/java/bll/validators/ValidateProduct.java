package bll.validators;

import model.Order;
import model.Product;

import javax.swing.*;

public class ValidateProduct{
    public ValidateProduct() {
    }
    public boolean validate(Product product) {
        if(product.getName().isEmpty()) {
            showError("Name is empty !");
            return false;
        }

        if(product.getDescription().isEmpty()) {
            showError("Description is empty !");
            return false;
        }

        if(product.getPrice() <= 0) {
            showError("Price is invalid !");
            return false;
        }

        if(product.getStock() <= 0) {
            showError("Stock is invalid !");
            return false;
        }

        return true;
    }
    public void showError(String error)
    {
        JFrame showError = new JFrame();
        JOptionPane.showMessageDialog(showError, error);
    }
}
