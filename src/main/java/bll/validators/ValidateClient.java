package bll.validators;

import model.Client;
import model.Order;

import javax.swing.*;
import java.util.regex.Pattern;

public class ValidateClient {
    private String emailRegex = "^[\\w-\\.]+@([\\w-]+\\.)+[\\w-]{2,4}$";
    private String phoneRegex = "^[\\+]?[(]?[0-9]{3}[)]?[-\\s\\.]?[0-9]{3}[-\\s\\.]?[0-9]{4,6}$";
    public ValidateClient(){}
    public boolean validate(Client client) {

        if(client.getFirst_name().isEmpty()) {
            showError("First name is empty !");
            return false;
        }

        if(client.getLast_name().isEmpty()) {
            showError("Last name is empty !");
            return false;
        }

        if(client.getAddress().isEmpty())
        {
            showError("Address is empty !");
            return false;
        }

        if(client.getEmail().isEmpty()) {
            showError("Email is empty !");
            return false;
        }

        if(client.getPhone_number().isEmpty()) {
            showError("Phone number is empty !");
            return false;
        }


        Pattern emailPattern = Pattern.compile(emailRegex);
        if(!emailPattern.matcher(client.getEmail()).matches())
        {
            showError("Email is invalid !");
            return false;
        }


        Pattern phonePattern = Pattern.compile(phoneRegex);
        if(!phonePattern.matcher(client.getPhone_number()).matches())
        {
            showError("Phone number is invalid !");
            return false;
        }

        return true;
    }

    public void showError(String error)
    {
        JFrame showError = new JFrame();
        JOptionPane.showMessageDialog(showError, error);
    }

}
