package bll.validators;

import bll.ClientBLL;
import bll.ProductBLL;
import model.Client;
import model.Order;
import model.Product;

import javax.swing.*;

public class ValidateOrder{

    public ValidateOrder(){}
    public boolean validate(Order order) {

        ClientBLL clientBLL = new ClientBLL();
        ProductBLL productBLL = new ProductBLL();

        Client client = clientBLL.findByIdClient(order.getId_client());
        Product product = productBLL.findByIdProduct(order.getId_product());

        if(client == null)
        {
            showError("Client with id " + order.getId_client() + " does not exist !");
            return false;
        }

        if(product == null)
        {
            showError("Product with id " + order.getId_product() + " does not exist !");
            return false;
        }

        if(order.getQuantity() <= 0)
        {
            showError("Quantity is invalid !");
            return false;
        }

        return true;
    }
    public void showError(String error)
    {
        JFrame showError = new JFrame();
        JOptionPane.showMessageDialog(showError, error);
    }
}
