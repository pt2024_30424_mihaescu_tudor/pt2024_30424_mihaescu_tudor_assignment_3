package bll;

import bll.validators.ValidateClient;
import dao.ClientDAO;
import model.Client;

import java.util.List;

public class ClientBLL {

    private ClientDAO clientDAO;
    public ClientBLL() {
        this.clientDAO = new ClientDAO();
    }

    public List<Client> findAllClients()
    {
        return clientDAO.findAll();
    }
    public Client findByIdClient(int id)
    {
        return clientDAO.findById(id);
    }
    public boolean insertClient(Client client)
    {
        ValidateClient validateClient = new ValidateClient();
        if(validateClient.validate(client))
        {
            clientDAO.insert(client);
            return true;
        }
        return false;
    }
    public boolean updateClient(Client client)
    {
        ValidateClient validateClient = new ValidateClient();
        if(validateClient.validate(client))
        {
            clientDAO.update(client);
            return true;
        }
        return false;
    }
    public void deleteClient(Client client)
    {
        clientDAO.delete(client);
    }

}
