package bll;

import dao.BillDAO;
import model.Bill;

import java.util.List;

public class BillBLL {
    private BillDAO billDAO;
    public BillBLL() {
        this.billDAO = new BillDAO();
    }

    public List<Bill> findAllBills()
    {
        return billDAO.findAll();
    }
    public boolean insertBill(Bill bill)
    {
        billDAO.insert(bill);
        return true;
    }
}
