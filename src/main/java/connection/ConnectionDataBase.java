package connection;

import javax.swing.*;
import javax.swing.plaf.nimbus.State;
import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ConnectionDataBase {
    private static final Logger logger = Logger.getLogger(ConnectionDataBase.class.getName());
    private static final String driver = "com.mysql.cj.jdbc.Driver";
    private static final String databaseURL = "jdbc:mysql://localhost:3306/order_management";
    private static final String user = "root";
    private static final String password = "240403";
    private static final ConnectionDataBase singleInstance = new ConnectionDataBase();

    private ConnectionDataBase()
    {
        try {
            Class.forName(driver);
        }
        catch (ClassNotFoundException e)
        {
            e.printStackTrace();
        }
    }

    public static Connection createConnection()
    {
        Connection connection = null;
        try {
            connection = DriverManager.getConnection(databaseURL, user, password);
        }
        catch (SQLException e)
        {
            logger.log(Level.WARNING, "An error occured while trying to connect to the database");
            e.printStackTrace();
        }
        return  connection;
    }

    public static Connection getConnection()
    {
        return singleInstance.createConnection();
    }

    public static void close(Connection connection)
    {
        if(connection != null)
        {
            try{
                connection.close();
            }
            catch (SQLException e)
            {
                logger.log(Level.WARNING, "An error occured while trying to close connection");
                e.printStackTrace();
            }
        }

    }

    public static void close(Statement statement)
    {
        if(statement != null)
        {
            try{
                statement.close();
            }
            catch (SQLException e)
            {
                logger.log(Level.WARNING, "An error occured while trying to close statement");
                e.printStackTrace();
            }
        }

    }

    public static void close(ResultSet resultSet)
    {
        if(resultSet != null)
        {
            try{
                resultSet.close();
            }
            catch (SQLException e)
            {
                logger.log(Level.WARNING, "An error occured while trying to close resultSet");
                e.printStackTrace();
            }
        }

    }


}
