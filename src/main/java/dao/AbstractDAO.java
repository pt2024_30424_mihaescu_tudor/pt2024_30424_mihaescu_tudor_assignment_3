package dao;

import connection.ConnectionDataBase;

import javax.swing.*;
import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.lang.reflect.*;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;

public class AbstractDAO<T> {

    private Logger logger = Logger.getLogger(AbstractDAO.class.getName());

    private Class<T> currentClass;

    @SuppressWarnings("unchecked")
    public AbstractDAO() {
        this.currentClass = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
    }


    //CREATE QUERRY STATEMENTS
    private String createSelectAll() {
        StringBuilder query = new StringBuilder();
        query.append("SELECT ");
        query.append(" * ");
        query.append(" FROM  `");
        query.append(currentClass.getSimpleName().toLowerCase());
        query.append("`");
        return query.toString();
    }

    private String createSelectQueryByField(String field) {
        StringBuilder query = new StringBuilder();
        query.append("SELECT ");
        query.append(" * ");
        query.append(" FROM `");
        query.append(currentClass.getSimpleName().toLowerCase());
        query.append("` WHERE " + field + " =?");
        return query.toString();
    }

    private String createInsertQuery() {
        StringBuilder query = new StringBuilder();
        query.append("INSERT INTO `");
        query.append(currentClass.getSimpleName().toLowerCase());
        query.append("` (");

        StringBuilder values = new StringBuilder();
        values.append(") VALUES (");

        boolean first = true;
        for (Field field : currentClass.getDeclaredFields()) {
            if (!field.getName().equals("id")) {
                if (!first) {
                    query.append(", ");
                    values.append(", ");
                }
                query.append("`").append(field.getName()).append("`");
                values.append("?");
                first = false;
            }
        }
        values.append(")");
        query.append(values);

        return query.toString();
    }

    private String createUpdateQuery() {
        StringBuilder query = new StringBuilder();
        query.append("UPDATE `");
        query.append(currentClass.getSimpleName().toLowerCase());
        query.append("` SET ");

        boolean first = true;
        for (Field field : currentClass.getDeclaredFields()) {
            if (!field.getName().equalsIgnoreCase("id")) {
                if (!first) {
                    query.append(", ");
                }
                query.append(field.getName()).append(" = ?");
                first = false;
            }

        }

        query.append(" WHERE id = ?");
        return query.toString();
    }

    private String createDeleteQuery() {
        StringBuilder query = new StringBuilder();
        query.append("DELETE FROM `");
        query.append(currentClass.getSimpleName().toLowerCase());
        query.append("` WHERE id = ? ");
        return query.toString();
    }


    //CREATE OBJECT / OBJECTS
    public T createObject(ResultSet resultSet) {
        T instance = null;
        try {
            if (!resultSet.next()) {
                return null;
            }

            Constructor<T> constructor = currentClass.getDeclaredConstructor();
            constructor.setAccessible(true);

            instance = constructor.newInstance();

            for (Field field : currentClass.getDeclaredFields()) {
                String fieldName = field.getName();
                Object value = resultSet.getObject(fieldName);

                PropertyDescriptor propertyDescriptor = new PropertyDescriptor(fieldName, currentClass);
                Method method = propertyDescriptor.getWriteMethod();
                method.setAccessible(true);
                method.invoke(instance, value);
            }

        } catch (SQLException e) {
            throw new RuntimeException("Error accessing data from ResultSet", e);
        } catch (NoSuchMethodException | InstantiationException | IllegalAccessException |
                 InvocationTargetException | IntrospectionException e) {
            throw new RuntimeException("Error creating instance of " + currentClass.getName(), e);
        }
        return instance;
    }


    public List<T> createObjects(ResultSet resultSet) {
        List<T> list = new ArrayList<>();
        Constructor[] constructors = currentClass.getDeclaredConstructors();
        Constructor currentConstructor = null;
        for (int i = 0; i < constructors.length; i++) {
            currentConstructor = constructors[i];
            if (currentConstructor.getGenericParameterTypes().length == 0) {
                break;
            }
        }

        try {
            while (resultSet.next()) {
                currentConstructor.setAccessible(true);
                T instance = (T) currentConstructor.newInstance();
                for (Field field : currentClass.getDeclaredFields()) {

                    field.setAccessible(true);
                    String fieldName = field.getName();
                    Object value = resultSet.getObject(fieldName);
                    PropertyDescriptor propertyDescriptor = new PropertyDescriptor(fieldName, currentClass);
                    Method method = propertyDescriptor.getWriteMethod();
                    if (method != null) {

                        method.invoke(instance, value);
                    }
                }
                list.add(instance);
            }

        } catch (SQLException e) {
            throw new RuntimeException(e);
        } catch (InvocationTargetException e) {
            throw new RuntimeException(e);
        } catch (InstantiationException e) {
            throw new RuntimeException(e);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        } catch (IntrospectionException e) {
            throw new RuntimeException(e);
        }
        return list;
    }

    //FIND ALL / BY
    public List<T> findAll() {
        List<T> list = new ArrayList<>();
        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;
        String query = createSelectAll();
        try {
            connection = ConnectionDataBase.getConnection();
            statement = connection.createStatement();
            resultSet = statement.executeQuery(query);

            return createObjects(resultSet);
        }
        catch (SQLException e) {
            showError("An error occured while findAll");
            logger.log(Level.WARNING, "An error occured while to findAll");
        }
        finally {
            if(statement != null)
            {
                ConnectionDataBase.close(statement);
            }
            if(resultSet != null)
            {
                ConnectionDataBase.close(resultSet);
            }
            if(connection != null)
            {
                ConnectionDataBase.close(connection);
            }

        }

        return list;
    }

    public T findById(int id) {

        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        String query = createSelectQueryByField("id");
        try {
            connection = ConnectionDataBase.getConnection();
            statement = connection.prepareStatement(query);
            statement.setInt(1, id);
            resultSet = statement.executeQuery();

            return createObject(resultSet);
        }
        catch (SQLException e) {
            showError("An error occured while to findByID");
            logger.log(Level.WARNING, "An error occured while to findByID");
        }
        finally {
            if(statement != null)
            {
                ConnectionDataBase.close(statement);
            }
            if(resultSet != null)
            {
                ConnectionDataBase.close(resultSet);
            }
            if(connection != null)
            {
                ConnectionDataBase.close(connection);
            }

        }

        return null;
    }

    //INSERT
    public T insert(T t) {
        Connection connection = null;
        PreparedStatement statement = null;
        String query = createInsertQuery();
        try {
            connection = ConnectionDataBase.getConnection();
            statement = connection.prepareStatement(query);
            int index = 0;
            for (Field field : currentClass.getDeclaredFields()) {
                field.setAccessible(true);
                try {
                    if(index == 0) {
                        index++;
                       continue;
                    }
                    statement.setObject(index, field.get(t));
                } catch (IllegalAccessException e) {
                    showError("Failed to access field value");
                    throw new RuntimeException("Failed to access field value", e);
                }
                index++;
            }

            int rowsModified = statement.executeUpdate();

            if (rowsModified == 0) {
                showError("Insert failed, no rows affected !");
                throw new SQLException("Insert failed, no rows affected !");
            }

        }
        catch (SQLException e) {
            showError("An error occurred while trying to insert");
            logger.log(Level.WARNING, "An error occurred while trying to insert", e);
            return null;
        }
        finally {
            if(statement != null)
            {
                ConnectionDataBase.close(statement);
            }
            if(connection != null)
            {
                ConnectionDataBase.close(connection);
            }

        }



        return null;
    }

    //UPDATE
    public T update(T t) {
        Connection connection = null;
        PreparedStatement statement = null;
        String query = createUpdateQuery();
        try {
            connection = ConnectionDataBase.getConnection();
            statement = connection.prepareStatement(query);

            int index = 1;
            Object idValue = null;

            for (Field field : currentClass.getDeclaredFields()) {
                field.setAccessible(true);
                if (!field.getName().equalsIgnoreCase("id")) {
                    statement.setObject(index++, field.get(t));
                } else {
                    idValue = field.get(t);
                }
            }

            if (idValue == null) {
                showError("ID value is null");
                throw new SQLException("ID value is null");
            }

            statement.setObject(index, idValue);

            int affectedRows = statement.executeUpdate();
            if (affectedRows == 0) {
                showError("Updating failed, no rows affected.");
                throw new SQLException("Updating failed, no rows affected.");
            }
        } catch (SQLException | IllegalAccessException e) {
            showError("An error occurred while trying to update");
            logger.log(Level.WARNING, "An error occurred while trying to update", e);
            return null;
        }

        return null;
    }

    public void delete(T t) {
        Connection connection = null;
        PreparedStatement statement = null;
        String query = createDeleteQuery();
        try {
            connection = ConnectionDataBase.getConnection();
            statement = connection.prepareStatement(query);

            Field idField = currentClass.getDeclaredField("id");
            idField.setAccessible(true);
            Object idValue = idField.get(t);

            if (idValue == null) {
                showError("ID value is null");
                throw new SQLException("ID value is null");
            }

            statement.setObject(1, idValue);

            int affectedRows = statement.executeUpdate();
            if (affectedRows == 0) {
                showError("Delete failed, no rows affected !");
                throw new SQLException("Delete failed, no rows affected.");
            }
        }
        catch (SQLException | IllegalAccessException e) {
            showError("An error occurred while trying to delete");
            logger.log(Level.WARNING, "An error occurred while trying to delete", e);
            return;
        }
        catch (NoSuchFieldException e) {
            throw new RuntimeException(e);
        }
        finally {
            if(statement != null)
            {
                ConnectionDataBase.close(statement);
            }
            if(connection != null)
            {
                ConnectionDataBase.close(connection);
            }
        }
    }


    //SHOW ERROR
    public void showError(String error)
    {
        JFrame showError = new JFrame();
        JOptionPane.showMessageDialog(showError, error);
    }

}
