package dao;

import connection.ConnectionDataBase;
import model.Bill;

import javax.swing.*;
import java.sql.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class BillDAO {

    private static final Logger logger = Logger.getLogger(BillDAO.class.getName());

    private String createInsertBillQuery() {
        return "INSERT INTO log (order_id, client_id, product_id, quantity, total_price, date) VALUES (?, ?, ?, ?, ?, ?)";
    }

    private String createSelectAllBillQuery() {
        return "SELECT * FROM log";
    }

    public List<Bill> createObjects(ResultSet resultSet) {
        List<Bill> list = new ArrayList<>();
        try {
            while (resultSet.next()) {
                int id = resultSet.getInt("id");
                int orderId = resultSet.getInt("order_id");
                int clientId = resultSet.getInt("client_id");
                int productId = resultSet.getInt("product_id");
                int quantity = resultSet.getInt("quantity");
                double totalPrice = resultSet.getDouble("total_price");
                Timestamp timestamp = resultSet.getTimestamp("date");
                LocalDateTime date = timestamp.toLocalDateTime();
                Bill bill = new Bill(id, orderId, clientId, productId, quantity, totalPrice, date);
                list.add(bill);
            }
        } catch (SQLException e) {
            logger.log(Level.SEVERE, "Error creating Bill objects from ResultSet", e);
            throw new RuntimeException(e);
        }
        return list;
    }

    public Bill insert(Bill bill) {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet generatedKeys = null;
        String query = createInsertBillQuery();
        try {
            connection = ConnectionDataBase.getConnection();
            statement = connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);

            statement.setInt(1, bill.orderId());
            statement.setInt(2, bill.clientId());
            statement.setInt(3, bill.productId());
            statement.setInt(4, bill.quantity());
            statement.setDouble(5, bill.totalPrice());
            statement.setTimestamp(6, Timestamp.valueOf(bill.date()));

            int rowsModified = statement.executeUpdate();

            if (rowsModified == 0) {
                showError("Insert failed, no rows affected!");
                throw new SQLException("Insert failed, no rows affected!");
            }

            generatedKeys = statement.getGeneratedKeys();
            if (generatedKeys.next()) {
                int generatedId = generatedKeys.getInt(1);
                return new Bill(generatedId, bill.orderId(), bill.clientId(), bill.productId(), bill.quantity(), bill.totalPrice(), bill.date());
            } else {
                throw new SQLException("Insert failed, no ID obtained.");
            }

        } catch (SQLException e) {
            showError("An error occurred while trying to insert the Bill");
            logger.log(Level.WARNING, "An error occurred while trying to insert", e);
            return null;
        } finally {
            if (generatedKeys != null) {
                ConnectionDataBase.close(generatedKeys);
            }
            if (statement != null) {
                ConnectionDataBase.close(statement);
            }
            if (connection != null) {
                ConnectionDataBase.close(connection);
            }
        }
    }

    public List<Bill> findAll() {
        List<Bill> list = new ArrayList<>();
        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;
        String query = createSelectAllBillQuery();
        try {
            connection = ConnectionDataBase.getConnection();
            statement = connection.createStatement();
            resultSet = statement.executeQuery(query);

            return createObjects(resultSet);
        } catch (SQLException e) {
            showError("An error occurred while retrieving all Bills");
            logger.log(Level.WARNING, "An error occurred while retrieving all Bills", e);
            return list;
        } finally {
            if (resultSet != null) {
                ConnectionDataBase.close(resultSet);
            }
            if (statement != null) {
                ConnectionDataBase.close(statement);
            }
            if (connection != null) {
                ConnectionDataBase.close(connection);
            }
        }
    }

    private void showError(String error) {
        JFrame showErrorFrame = new JFrame();
        JOptionPane.showMessageDialog(showErrorFrame, error);
    }
}
