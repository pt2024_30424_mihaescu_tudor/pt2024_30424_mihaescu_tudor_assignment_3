package presentation;

import bll.BillBLL;
import bll.ClientBLL;
import bll.OrderBLL;
import bll.ProductBLL;
import model.Bill;
import model.Client;
import model.Order;
import model.Product;

import presentation.Panels.InsertPanel;
import presentation.Panels.UpdatePanel;
import presentation.Panels.DeletePanel;
import presentation.TableModel.BillTableModel;
import presentation.TableModel.ClientTableModel;
import presentation.TableModel.OrderTableModel;
import presentation.TableModel.ProductTableModel;

import javax.swing.*;
import java.awt.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

public class MainGUI {
    private ClientBLL clientBLL;
    private ProductBLL productBLL;
    private OrderBLL orderBLL;
    private BillBLL billBLL;
    private JTable clientTable;
    private JTable orderTable;
    private JTable productTable;
    private JTable billTable;
    private JTable currentTable;
    private JFrame frame;
    private JTabbedPane tabbedPane;

    public MainGUI() {
        this.frame = new JFrame();
        this.clientBLL = new ClientBLL();
        this.productBLL = new ProductBLL();
        this.orderBLL = new OrderBLL();
        this.billBLL = new BillBLL();

        frame.setTitle("Order Management");
        frame.setSize(800, 600);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLayout(new BorderLayout());

        initPanels();
    }

    private void initPanels() {
        tabbedPane = new JTabbedPane();
        tabbedPane.add("Clients", createClientPanel());
        tabbedPane.add("Product", createProductPanel());
        tabbedPane.add("Orders", createOrderPanel());
        tabbedPane.add("Log", createBillPanel());

        currentTable = clientTable;

        frame.add(tabbedPane, BorderLayout.CENTER);

        tabbedPane.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                handleUpdateTab();
            }
        });

        JButton updateButton = new JButton("Update");
        updateButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                handleUpdateAction();
            }
        });

        JButton deleteButton = new JButton("Delete");
        deleteButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                handleDeleteAction();
            }
        });

        JButton insertButton = new JButton("Insert");
        insertButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                handleInsertAction();
            }
        });

        JPanel buttonPanel = new JPanel(new FlowLayout());
        buttonPanel.add(insertButton);
        buttonPanel.add(updateButton);
        buttonPanel.add(deleteButton);

        frame.add(buttonPanel, BorderLayout.SOUTH);
        frame.setVisible(true);
    }

    private JPanel createClientPanel() {
        List<Client> clients = clientBLL.findAllClients();
        ClientTableModel clientTableModel = new ClientTableModel(clients);
        clientTable = new JTable(clientTableModel);

        JScrollPane scrollPane = new JScrollPane(clientTable);
        JPanel panel = new JPanel(new BorderLayout());
        panel.add(scrollPane, BorderLayout.CENTER);

        return panel;
    }

    private JPanel createProductPanel() {
        List<Product> products = productBLL.findAllProduct();
        ProductTableModel productTableModel = new ProductTableModel(products);
        productTable = new JTable(productTableModel);
        JScrollPane scrollPane = new JScrollPane(productTable);
        JPanel panel = new JPanel(new BorderLayout());
        panel.add(scrollPane, BorderLayout.CENTER);
        return panel;
    }

    private JPanel createOrderPanel() {
        List<Order> orders = orderBLL.findAllOrder();
        OrderTableModel orderTableModel = new OrderTableModel(orders);
        orderTable = new JTable(orderTableModel);
        JScrollPane scrollPane = new JScrollPane(orderTable);
        JPanel panel = new JPanel(new BorderLayout());
        panel.add(scrollPane, BorderLayout.CENTER);
        return panel;
    }

    private JPanel createBillPanel() {
        List<Bill> bills = billBLL.findAllBills();
        BillTableModel billTableModel = new BillTableModel(bills);
        billTable = new JTable(billTableModel);
        JScrollPane scrollPane = new JScrollPane(billTable);
        JPanel panel = new JPanel(new BorderLayout());
        panel.add(scrollPane, BorderLayout.CENTER);
        return panel;
    }

    public void refreshClientPanel() {
        List<Client> clients = clientBLL.findAllClients();
        ClientTableModel clientTableModel = new ClientTableModel(clients);
        currentTable.setModel(clientTableModel);
        currentTable.repaint();
    }

    public void refreshProductPanel() {
        List<Product> products = productBLL.findAllProduct();
        ProductTableModel productTableModel = new ProductTableModel(products);
        currentTable.setModel(productTableModel);
        currentTable.repaint();
    }

    public void refreshOrderPanel() {
        List<Order> orders = orderBLL.findAllOrder();
        OrderTableModel orderTableModel = new OrderTableModel(orders);
        currentTable.setModel(orderTableModel);
        currentTable.repaint();
    }

    public void refreshBillPanel() {
        List<Bill> bills = billBLL.findAllBills();
        BillTableModel billTableModel = new BillTableModel(bills);
        currentTable.setModel(billTableModel);
        currentTable.repaint();
    }

    private void handleUpdateTab() {
        int selectedIndex = tabbedPane.getSelectedIndex();
        switch (selectedIndex) {
            case 0:
                currentTable = clientTable;
                refreshClientPanel();
                break;
            case 1:
                currentTable = productTable;
                refreshProductPanel();
                break;
            case 2:
                currentTable = orderTable;
                refreshOrderPanel();
                break;
            case 3:
                currentTable = billTable;
                refreshBillPanel();
                break;
        }
    }

    private void handleUpdateAction() {
        if (currentTable != null) {
            int selectedRow = currentTable.getSelectedRow();
            if (selectedRow != -1) {
                if (tabbedPane.getSelectedIndex() == 0) {
                    ClientTableModel clientTableModel = (ClientTableModel) currentTable.getModel();
                    Client selectedClient = clientTableModel.getClientAt(selectedRow);
                    new UpdatePanel<Client>(selectedClient, this);
                } else if (tabbedPane.getSelectedIndex() == 1) {
                    ProductTableModel productTableModel = (ProductTableModel) currentTable.getModel();
                    Product selectedProduct = productTableModel.getProductAt(selectedRow);
                    new UpdatePanel<Product>(selectedProduct, this);
                } else if (tabbedPane.getSelectedIndex() == 2) {
                    OrderTableModel orderTableModel = (OrderTableModel) currentTable.getModel();
                    Order selectedOrder = orderTableModel.getOrderAt(selectedRow);
                    new UpdatePanel<Order>(selectedOrder, this);
                }
            } else {
                JOptionPane.showMessageDialog(frame, "Please select a row to update.");
            }
        }
    }

    private void handleDeleteAction() {
        if (currentTable != null) {
            int selectedRow = currentTable.getSelectedRow();
            if (selectedRow != -1) {
                if (tabbedPane.getSelectedIndex() == 0) {
                    ClientTableModel clientTableModel = (ClientTableModel) currentTable.getModel();
                    Client selectedClient = clientTableModel.getClientAt(selectedRow);
                    new DeletePanel<Client>(selectedClient, this);
                } else if (tabbedPane.getSelectedIndex() == 1) {
                    ProductTableModel productTableModel = (ProductTableModel) currentTable.getModel();
                    Product selectedProduct = productTableModel.getProductAt(selectedRow);
                    new DeletePanel<Product>(selectedProduct, this);
                } else if (tabbedPane.getSelectedIndex() == 2) {
                    OrderTableModel orderTableModel = (OrderTableModel) currentTable.getModel();
                    Order selectedOrder = orderTableModel.getOrderAt(selectedRow);
                    new DeletePanel<Order>(selectedOrder, this);
                }
            } else {
                JFrame showError = new JFrame();
                JOptionPane.showMessageDialog(showError, "Please select a row to update.");
            }
        }
    }

    private void handleInsertAction() {
        if (currentTable != null) {
            if (tabbedPane.getSelectedIndex() == 0) {
                new InsertPanel<Client>(new Client(), this);
            } else if (tabbedPane.getSelectedIndex() == 1) {
                new InsertPanel<Product>(new Product(), this);
            } else if (tabbedPane.getSelectedIndex() == 2) {
                new InsertPanel<Order>(new Order(), this);
            }
        }
    }
}
