package presentation.TableModel;

import model.Bill;

import javax.swing.table.AbstractTableModel;
import java.util.List;

public class BillTableModel extends AbstractTableModel {
    private final String[] columnNames = {"ID", "Order Id", "Client Id", "Product Id", "Quantity", "Total Price", "Date"};
    private List<Bill> bills;

    public BillTableModel(List<Bill> bills) {
        this.bills = bills;
    }

    @Override
    public int getRowCount() {
        return bills.size();
    }

    @Override
    public int getColumnCount() {
        return columnNames.length;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Bill bill = bills.get(rowIndex);
        switch (columnIndex) {
            case 0: return bill.id();
            case 1: return bill.orderId();
            case 2: return bill.clientId();
            case 3: return bill.productId();
            case 4: return bill.quantity();
            case 5: return bill.totalPrice();
            case 6: return bill.date().toString();
            default: return null;
        }
    }

    @Override
    public String getColumnName(int column) {
        return columnNames[column];
    }

    public Bill getBillAt(int selectedRow) {
        return bills.get(selectedRow);
    }
}
