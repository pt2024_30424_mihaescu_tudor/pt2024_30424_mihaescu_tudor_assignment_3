package presentation.TableModel;

import bll.ClientBLL;
import bll.ProductBLL;
import model.Client;
import model.Order;
import model.Product;

import javax.swing.table.AbstractTableModel;
import java.util.List;

public class OrderTableModel extends AbstractTableModel {
    private final String[] columnNames = {"ID", "CLIENT ID", "CLIENT NAME", "PRODUCT ID", "PRODUCT NAME", "DELIVERY ADDRESS", "QUANTITY", "TOTAL PRICE"};
    private List<Order> orders;
    private ClientBLL clientBLL;
    private ProductBLL productBLL;

    public OrderTableModel(List<Order> orders) {
        this.orders = orders;
        this.clientBLL = new ClientBLL();
        this.productBLL = new ProductBLL();
    }

    @Override
    public int getRowCount() {
        return orders.size();
    }

    @Override
    public int getColumnCount() {
        return columnNames.length;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Order order = orders.get(rowIndex);
        switch (columnIndex) {
            case 0: return order.getId();
            case 1: return order.getId_client();
            case 2: return getClientName(order);
            case 3: return order.getId_product();
            case 4: return getProductName(order);
            case 5: return order.getDelivery_address();
            case 6: return order.getQuantity();
            case 7: return order.getTotal_price();
            default: return null;
        }
    }

    private String getClientName(Order order) {
        Client client = clientBLL.findByIdClient(order.getId_client());
        return client.getFirst_name() + " " + client.getLast_name();
    }

    private String getProductName(Order order) {
        Product product = productBLL.findByIdProduct(order.getId_product());
        return product.getName();
    }

    @Override
    public String getColumnName(int column) {
        return columnNames[column];
    }

    public Order getOrderAt(int selectedRow) {
        return orders.get(selectedRow);
    }
}
