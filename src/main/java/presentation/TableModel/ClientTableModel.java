package presentation.TableModel;

import model.Client;

import javax.swing.table.AbstractTableModel;
import java.util.List;

public class ClientTableModel extends AbstractTableModel {
    private final String[] columnNames = {"ID", "First Name", "Last Name", "Address", "Email", "Phone Number"};
    private List<Client> clients;

    public ClientTableModel(List<Client> clients) {
        this.clients = clients;
    }

    @Override
    public int getRowCount() {
        return clients.size();
    }

    @Override
    public int getColumnCount() {
        return columnNames.length;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Client client = clients.get(rowIndex);
        switch (columnIndex) {
            case 0: return client.getId();
            case 1: return client.getFirst_name();
            case 2: return client.getLast_name();
            case 3: return client.getAddress();
            case 4: return client.getEmail();
            case 5: return client.getPhone_number();
            default: return null;
        }
    }

    @Override
    public String getColumnName(int column) {
        return columnNames[column];
    }

    /**
     * Gets the client object at the specified row.
     * @param selectedRow The index of the selected row.
     * @return The Client object at the specified row.
     */
    public Client getClientAt(int selectedRow) {
        return clients.get(selectedRow);
    }
}
