package presentation.TableModel;

import model.Order;
import model.Product;

import javax.swing.table.AbstractTableModel;
import java.util.List;


public class ProductTableModel extends AbstractTableModel {
    private final String[] columnNames = {"ID", "PRODUCT NAME", "DESCRIPTION", "PRICE", "STOCK"};
    private List<Product> products;
    public ProductTableModel(List<Product> products) {
        this.products = products;
    }

    @Override
    public int getRowCount() {
        return products.size();
    }

    @Override
    public int getColumnCount() {
        return columnNames.length;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Product product = products.get(rowIndex);
        switch (columnIndex) {
            case 0: return product.getId();
            case 1: return product.getName();
            case 2: return product.getDescription();
            case 3: return product.getPrice();
            case 4: return product.getStock();
            default: return null;
        }
    }

    @Override
    public String getColumnName(int column) {
        return columnNames[column];
    }

    public Product getProductAt(int selectedRow) {
        return products.get(selectedRow);
    }
}
