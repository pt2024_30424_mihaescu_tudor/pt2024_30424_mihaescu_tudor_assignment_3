package presentation.Panels;

import bll.ClientBLL;
import bll.OrderBLL;
import bll.ProductBLL;
import model.Client;
import model.Order;
import model.Product;
import presentation.MainGUI;
import start.Main;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class UpdatePanel<T> extends JFrame {

    private MainGUI mainGUI;
    private T currentClass;
    private JTextField firstNameFieldClient;
    private JTextField lastNameFieldClient;
    private JTextField addressFieldClient;
    private JTextField emailFieldClient;
    private JTextField phoneNumberFieldClient;

    private JTextField nameFieldProduct;
    private JTextField descriptionFieldProduct;
    private JTextField priceFieldProduct;


    private JTextField idClientFieldOrder;
    private JTextField idProductFieldOrder;
    private JTextField deliveryAddressFieldOrder;
    private JTextField totalPriceFieldOrder;

    public UpdatePanel(T currentClass, MainGUI mainGUI) {
        this.currentClass = currentClass;
        this.mainGUI = mainGUI;
        setTitle("Update");
        setSize(400, 200);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        init();
        setVisible(true);
    }

    private void init() {
        if (currentClass instanceof Client) {
            initComponentsClient((Client) currentClass);
            addComponentsClient((Client) currentClass);
            updateClient((Client) currentClass);
        } else if (currentClass instanceof Product) {
            initComponentsProduct((Product) currentClass);
            addComponentsProduct((Product) currentClass);
            updateProduct((Product) currentClass);
        } else if (currentClass instanceof Order) {
            initComponentsOrder((Order) currentClass);
            addComponentsOrder((Order) currentClass);
            updateOrder((Order) currentClass);
        }
    }

    private void initComponentsClient(Client client) {
        this.firstNameFieldClient = new JTextField(client.getFirst_name());
        this.lastNameFieldClient = new JTextField(client.getLast_name());
        this.addressFieldClient = new JTextField(client.getAddress());
        this.emailFieldClient = new JTextField(client.getEmail());
        this.phoneNumberFieldClient = new JTextField(client.getPhone_number());
    }

    private void addComponentsClient(Client client) {
        JPanel updatePanel = new JPanel(new GridLayout(5, 2));
        updatePanel.add(new JLabel("First Name:"));
        updatePanel.add(firstNameFieldClient);
        updatePanel.add(new JLabel("Last Name:"));
        updatePanel.add(lastNameFieldClient);
        updatePanel.add(new JLabel("Address:"));
        updatePanel.add(addressFieldClient);
        updatePanel.add(new JLabel("Email:"));
        updatePanel.add(emailFieldClient);
        updatePanel.add(new JLabel("Phone Number:"));
        updatePanel.add(phoneNumberFieldClient);

        JButton okButton = new JButton("OK");
        okButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                updateClient(client);
            }
        });

        JButton cancelButton = new JButton("Cancel");
        cancelButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });

        JPanel buttonPanel = new JPanel();
        buttonPanel.add(okButton);
        buttonPanel.add(cancelButton);

        setLayout(new BorderLayout());
        add(updatePanel, BorderLayout.CENTER);
        add(buttonPanel, BorderLayout.SOUTH);
    }

    private void updateClient(Client client) {
        client.setFirst_name(firstNameFieldClient.getText());
        client.setLast_name(lastNameFieldClient.getText());
        client.setAddress(addressFieldClient.getText());
        client.setEmail(emailFieldClient.getText());
        client.setPhone_number(phoneNumberFieldClient.getText());
        ClientBLL clientBLL = new ClientBLL();
        if (clientBLL.updateClient(client)) {
            mainGUI.refreshClientPanel();
            dispose();
        }
    }


    private void initComponentsProduct(Product product) {
        this.nameFieldProduct = new JTextField(product.getName());
        this.descriptionFieldProduct = new JTextField(product.getDescription());
        this.priceFieldProduct = new JTextField(String.valueOf(product.getPrice()));
    }

    private void addComponentsProduct(Product product) {
        JPanel updatePanel = new JPanel(new GridLayout(5, 2));
        updatePanel.add(new JLabel("Product Name:"));
        updatePanel.add(nameFieldProduct);
        updatePanel.add(new JLabel("Description:"));
        updatePanel.add(descriptionFieldProduct);
        updatePanel.add(new JLabel("Price:"));
        updatePanel.add(priceFieldProduct);

        JButton okButton = new JButton("OK");
        okButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                updateProduct(product);

            }
        });

        JButton cancelButton = new JButton("Cancel");
        cancelButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });

        JPanel buttonPanel = new JPanel();
        buttonPanel.add(okButton);
        buttonPanel.add(cancelButton);

        setLayout(new BorderLayout());
        add(updatePanel, BorderLayout.CENTER);
        add(buttonPanel, BorderLayout.SOUTH);
    }

    private void updateProduct(Product product) {
        product.setName(nameFieldProduct.getText());
        product.setDescription(descriptionFieldProduct.getText());
        product.setPrice(Integer.parseInt(priceFieldProduct.getText()));
        ProductBLL productBLL = new ProductBLL();
        if (productBLL.updateProduct(product)) {
            mainGUI.refreshProductPanel();
            dispose();
        }
    }


    private void initComponentsOrder(Order order) {
        this.idClientFieldOrder = new JTextField(String.valueOf(order.getId_client()));
        this.idProductFieldOrder = new JTextField(String.valueOf(order.getId_product()));
        this.deliveryAddressFieldOrder = new JTextField(order.getDelivery_address());
        this.totalPriceFieldOrder = new JTextField(String.valueOf(order.getTotal_price()));
    }

    private void addComponentsOrder(Order order) {
        JPanel updatePanel = new JPanel(new GridLayout(4, 2));
        updatePanel.add(new JLabel("Id Client:"));
        updatePanel.add(idClientFieldOrder);
        updatePanel.add(new JLabel("Id Product:"));
        updatePanel.add(idProductFieldOrder);
        updatePanel.add(new JLabel("Delivery Address:"));
        updatePanel.add(deliveryAddressFieldOrder);
        updatePanel.add(new JLabel("Total Price:"));
        updatePanel.add(totalPriceFieldOrder);

        JButton okButton = new JButton("OK");
        okButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                updateOrder(order);
            }
        });

        JButton cancelButton = new JButton("Cancel");
        cancelButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });

        JPanel buttonPanel = new JPanel();
        buttonPanel.add(okButton);
        buttonPanel.add(cancelButton);

        setLayout(new BorderLayout());
        add(updatePanel, BorderLayout.CENTER);
        add(buttonPanel, BorderLayout.SOUTH);
    }

    private void updateOrder(Order order) {
        order.setId_client(Integer.parseInt(idClientFieldOrder.getText()));
        order.setId_product(Integer.parseInt(idProductFieldOrder.getText()));
        order.setDelivery_address(deliveryAddressFieldOrder.getText());
        order.setTotal_price(Integer.parseInt(totalPriceFieldOrder.getText()));
        OrderBLL orderBLL = new OrderBLL();
        if (orderBLL.updateOrder(order)) {
            mainGUI.refreshOrderPanel();
            mainGUI.refreshBillPanel();
            dispose();
        }
    }
}
