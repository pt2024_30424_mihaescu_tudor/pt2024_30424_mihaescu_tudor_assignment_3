package presentation.Panels;

import bll.ClientBLL;
import bll.OrderBLL;
import bll.ProductBLL;
import model.Client;
import model.Order;
import model.Product;
import presentation.MainGUI;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class DeletePanel<T> extends JFrame {

    private MainGUI mainGUI;
    private T currentClass;

    public DeletePanel(T currentClass, MainGUI mainGUI) {
        this.currentClass = currentClass;
        this.mainGUI = mainGUI;
        setTitle("Delete");
        setSize(300, 150);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        addComponents();
        setVisible(true);
    }

    private void addComponents() {
        JPanel updatePanel = new JPanel(new GridLayout(2, 1));
        updatePanel.add(new JLabel("Are you sure you want to delete this record?"), BorderLayout.CENTER);

        JButton okButton = new JButton("YES");
        okButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                deleteRecord();
            }
        });

        JButton cancelButton = new JButton("NO");
        cancelButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });

        JPanel buttonPanel = new JPanel();
        buttonPanel.add(okButton);
        buttonPanel.add(cancelButton);

        setLayout(new BorderLayout());
        add(updatePanel, BorderLayout.CENTER);
        add(buttonPanel, BorderLayout.SOUTH);
    }

    private void deleteRecord() {
        if (currentClass instanceof Client) {
            ClientBLL clientBLL = new ClientBLL();
            clientBLL.deleteClient((Client) currentClass);
            mainGUI.refreshClientPanel();
        } else if (currentClass instanceof Product) {
            ProductBLL productBLL = new ProductBLL();
            productBLL.deleteProduct((Product) currentClass);
            mainGUI.refreshProductPanel();
        } else if (currentClass instanceof Order) {
            OrderBLL orderBLL = new OrderBLL();
            orderBLL.deleteOrder((Order) currentClass);
            mainGUI.refreshOrderPanel();
        }
        dispose();
    }
}
