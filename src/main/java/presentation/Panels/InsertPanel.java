package presentation.Panels;

import bll.ClientBLL;
import bll.OrderBLL;
import bll.ProductBLL;
import model.Client;
import model.Order;
import model.Product;
import presentation.MainGUI;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class InsertPanel<T> extends JFrame {

    private MainGUI mainGUI;
    private T currentClass;
    private JTextField firstNameFieldClient;
    private JTextField lastNameFieldClient;
    private JTextField addressFieldClient;
    private JTextField emailFieldClient;
    private JTextField phoneNumberFieldClient;

    private JTextField nameFieldProduct;
    private JTextField descriptionFieldProduct;
    private JTextField priceFieldProduct;
    private JTextField stockFieldProduct;


    private JTextField idClientFieldOrder;
    private JTextField idProductFieldOrder;
    private JTextField deliveryAddressFieldOrder;
    private JTextField quantityFieldOrder;

    public InsertPanel(T currentClass, MainGUI mainGUI) {
        this.currentClass = currentClass;
        this.mainGUI = mainGUI;
        setTitle("Insert");
        setSize(400, 200);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        init();
        setVisible(true);
    }

    private void init() {
        if (currentClass instanceof Client) {
            initComponentsClient();
            addComponentsClient((Client) currentClass);
        } else if (currentClass instanceof Product) {
            initComponentsProduct();
            addComponentsProduct((Product) currentClass);
        } else if (currentClass instanceof Order) {
            initComponentsOrder();
            addComponentsOrder((Order) currentClass);
        }
    }

    private void initComponentsClient() {
        this.firstNameFieldClient = new JTextField();
        this.lastNameFieldClient = new JTextField();
        this.addressFieldClient = new JTextField();
        this.emailFieldClient = new JTextField();
        this.phoneNumberFieldClient = new JTextField();
    }

    private void addComponentsClient(Client client) {
        JPanel updatePanel = new JPanel(new GridLayout(5, 2));
        updatePanel.add(new JLabel("First Name:"));
        updatePanel.add(firstNameFieldClient);
        updatePanel.add(new JLabel("Last Name:"));
        updatePanel.add(lastNameFieldClient);
        updatePanel.add(new JLabel("Address:"));
        updatePanel.add(addressFieldClient);
        updatePanel.add(new JLabel("Email:"));
        updatePanel.add(emailFieldClient);
        updatePanel.add(new JLabel("Phone Number:"));
        updatePanel.add(phoneNumberFieldClient);

        JButton okButton = new JButton("OK");
        okButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                insertClient(client);
            }
        });

        JButton cancelButton = new JButton("Cancel");
        cancelButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });

        JPanel buttonPanel = new JPanel();
        buttonPanel.add(okButton);
        buttonPanel.add(cancelButton);

        setLayout(new BorderLayout());
        add(updatePanel, BorderLayout.CENTER);
        add(buttonPanel, BorderLayout.SOUTH);
    }

    private void insertClient(Client client) {
        client.setFirst_name(firstNameFieldClient.getText());
        client.setLast_name(lastNameFieldClient.getText());
        client.setAddress(addressFieldClient.getText());
        client.setEmail(emailFieldClient.getText());
        client.setPhone_number(phoneNumberFieldClient.getText());
        ClientBLL clientBLL = new ClientBLL();
        if (clientBLL.insertClient(client)) {
            mainGUI.refreshClientPanel();
            dispose();
        }
    }


    private void initComponentsProduct() {
        this.nameFieldProduct = new JTextField();
        this.descriptionFieldProduct = new JTextField();
        this.priceFieldProduct = new JTextField();
        this.stockFieldProduct = new JTextField();
    }

    private void addComponentsProduct(Product product) {
        JPanel updatePanel = new JPanel(new GridLayout(5, 2));
        updatePanel.add(new JLabel("Product Name:"));
        updatePanel.add(nameFieldProduct);
        updatePanel.add(new JLabel("Description:"));
        updatePanel.add(descriptionFieldProduct);
        updatePanel.add(new JLabel("Price:"));
        updatePanel.add(priceFieldProduct);
        updatePanel.add(new JLabel("Stock:"));
        updatePanel.add(stockFieldProduct);


        JButton okButton = new JButton("OK");
        okButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                insertProduct(product);

            }
        });

        JButton cancelButton = new JButton("Cancel");
        cancelButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });

        JPanel buttonPanel = new JPanel();
        buttonPanel.add(okButton);
        buttonPanel.add(cancelButton);

        setLayout(new BorderLayout());
        add(updatePanel, BorderLayout.CENTER);
        add(buttonPanel, BorderLayout.SOUTH);
    }

    private void insertProduct(Product product) {
        product.setName(nameFieldProduct.getText());
        product.setDescription(descriptionFieldProduct.getText());
        product.setPrice(Integer.parseInt(priceFieldProduct.getText()));
        product.setStock(Integer.parseInt(stockFieldProduct.getText()));
        ProductBLL productBLL = new ProductBLL();
        if(productBLL.insertProduct(product))
        {
            mainGUI.refreshProductPanel();
            dispose();
        }
    }

    private void initComponentsOrder() {
        this.idClientFieldOrder = new JTextField();
        this.idProductFieldOrder = new JTextField();
        this.deliveryAddressFieldOrder = new JTextField();
        this.quantityFieldOrder = new JTextField();
    }

    private void addComponentsOrder(Order order) {
        JPanel updatePanel = new JPanel(new GridLayout(4, 2));
        updatePanel.add(new JLabel("Id Client:"));
        updatePanel.add(idClientFieldOrder);
        updatePanel.add(new JLabel("Id Product:"));
        updatePanel.add(idProductFieldOrder);
        updatePanel.add(new JLabel("Delivery Address:"));
        updatePanel.add(deliveryAddressFieldOrder);
        updatePanel.add(new JLabel("Quantity:"));
        updatePanel.add(quantityFieldOrder);

        JButton okButton = new JButton("OK");
        okButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                insertOrder(order);
            }
        });

        JButton cancelButton = new JButton("Cancel");
        cancelButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });

        JPanel buttonPanel = new JPanel();
        buttonPanel.add(okButton);
        buttonPanel.add(cancelButton);

        setLayout(new BorderLayout());
        add(updatePanel, BorderLayout.CENTER);
        add(buttonPanel, BorderLayout.SOUTH);
    }

    private void insertOrder(Order order) {
        order.setId_client(Integer.parseInt(idClientFieldOrder.getText()));
        order.setId_product(Integer.parseInt(idProductFieldOrder.getText()));
        order.setDelivery_address(deliveryAddressFieldOrder.getText());
        order.setQuantity(Integer.parseInt(quantityFieldOrder.getText()));
        order.setTotal_price(0);
        OrderBLL orderBLL = new OrderBLL();
        if (orderBLL.insertOrder(order)) {
            mainGUI.refreshOrderPanel();
            mainGUI.refreshBillPanel();
            dispose();
        }
    }
}
