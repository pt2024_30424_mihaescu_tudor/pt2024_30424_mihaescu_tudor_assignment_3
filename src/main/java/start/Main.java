package start;

import presentation.MainGUI;

import javax.swing.*;
import java.awt.*;
import java.util.List;

/**
 *
 *
 */

public class Main {

    public static void main(String[] args) {
        SwingUtilities.invokeLater(() -> {
            MainGUI frame = new MainGUI();
        });
    }

}
